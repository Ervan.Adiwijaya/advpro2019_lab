package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class UiUxDesigner extends Employees {
    public UiUxDesigner(String name, double salary) throws IllegalArgumentException {
        super(name, salary, "UI/UX Designer", 90000);
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
