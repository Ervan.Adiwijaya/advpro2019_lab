package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.Company;
import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;

import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

public class CompositeUse {
    public static void main(String[] args) {
        Company misan = new Company();

        Employees kani = new Ceo("kani", 500000);
        Employees gagah = new Cto("gagah", 500000);
        Employees apoi = new NetworkExpert("apoi", 500000);
        Employees usman = new BackendProgrammer("usman", 500000);
        Employees gio = new FrontendProgrammer("gio", 500000);
        Employees aab = new SecurityExpert("kani", 500000);
        Employees cloud = new UiUxDesigner("cloud", 500000); 
        Employees ariq = new FrontendProgrammer("ariq", 500000);

        misan.addEmployee(kani);
        misan.addEmployee(gagah);
        misan.addEmployee(apoi);
        misan.addEmployee(usman);
        misan.addEmployee(gio);
        misan.addEmployee(aab);
        misan.addEmployee(cloud);
        misan.addEmployee(ariq);

        System.out.println("=== SELAMAT DATANG DI MISAN CORP ===\n");
        System.out.println("ANGGOTA PERUSAHAAN :");
        for (int i = 0; i < misan.getAllEmployees().size(); i++) {
            Employees temp = misan.getAllEmployees().get(i);
            System.out.println(temp.getName() + ", sebagai " + temp.getRole() 
                + " bergaji " + temp.getSalary());
        }

        System.out.print("\n\n");

        System.out.println("Total gaji perusahaan : " + misan.getNetSalaries());
    }
}