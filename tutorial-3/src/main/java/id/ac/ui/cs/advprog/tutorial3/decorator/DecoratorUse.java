package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class DecoratorUse {

    public static void main(String[] args) {

        Food sandwich_crust = BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();
        Food sandwich_non_crust = BreadProducer.NO_CRUST_SANDWICH.createBreadToBeFilled();

        Food tomato_sandwich = FillingDecorator.TOMATO.addFillingToBread(sandwich_non_crust);
        Food tomato_cheese_sandwich = FillingDecorator.CHEESE.addFillingToBread(tomato_sandwich);

        System.out.println(tomato_cheese_sandwich.getDescription());
        System.out.println(tomato_cheese_sandwich.cost());
    }

}
