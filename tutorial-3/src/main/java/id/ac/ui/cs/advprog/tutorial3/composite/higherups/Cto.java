package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {
    public Cto(String name, double salary) throws IllegalArgumentException {
        super(name, salary, "CTO", 100000);
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
