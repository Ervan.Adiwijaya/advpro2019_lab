package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Ceo extends Employees {
    public Ceo(String name, double salary) throws IllegalArgumentException {
        super(name, salary, "CEO", 200000);
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
