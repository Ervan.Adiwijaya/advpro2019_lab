package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class BackendProgrammer extends Employees {
    public BackendProgrammer(String name, double salary) throws IllegalArgumentException {
        super(name, salary, "Back End Programmer", 20000);
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
