package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class NetworkExpert extends Employees {
    public NetworkExpert(String name, double salary) throws IllegalArgumentException {
        super(name, salary, "Network Expert", 50000);
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
