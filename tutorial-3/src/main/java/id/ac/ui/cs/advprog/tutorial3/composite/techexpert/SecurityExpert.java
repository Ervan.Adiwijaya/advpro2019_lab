package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class SecurityExpert extends Employees {
    public SecurityExpert(String name, double salary)throws IllegalArgumentException {
        super(name, salary, "Security Expert", 70000);
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
