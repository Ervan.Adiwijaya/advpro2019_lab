package id.ac.ui.cs.advprog.tutorial3.composite;

public abstract class Employees {
    protected String name = "Unidentified Name";
    protected double salary;
    protected String role;
    protected double minSalary;

    public Employees(String name, double salary, String role, double minSalary) 
        throws IllegalArgumentException {
        if (salary < minSalary) {
            throw new IllegalArgumentException();
        } else {
            this.name = name;
            this.salary = salary;
            this.role = role;
        }
    }

    public String getName() {
        return this.name;
    }

    public abstract double getSalary();

    public String getRole() {
        return this.role;
    }
}
