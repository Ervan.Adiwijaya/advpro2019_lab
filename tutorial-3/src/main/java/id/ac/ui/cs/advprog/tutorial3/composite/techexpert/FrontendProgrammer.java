package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class FrontendProgrammer extends Employees {
    public FrontendProgrammer(String name, double salary) throws IllegalArgumentException {
        super(name, salary, "Front End Programmer", 30000);
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
