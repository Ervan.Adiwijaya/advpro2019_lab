package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class MacroCommand implements Command {

    private List<Command> commands;

    public MacroCommand(Command[] commands) {
        this.commands = Arrays.asList(commands);
    }

    @Override
    public void execute() {
        this.commands.stream().forEach(commands -> commands.execute());
    }

    @Override
    public void undo() {
        this.commands.stream().collect(Collectors
                              .collectingAndThen(Collectors.toList(), 
                                  list -> { 
                                      Collections.reverse(list); 
                                      return list; } 
                                        )
                                      )
                              .forEach(commands -> commands.undo());
    }
}
