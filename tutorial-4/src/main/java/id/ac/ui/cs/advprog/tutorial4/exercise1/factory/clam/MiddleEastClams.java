package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class MiddleEastClams implements Clams {

    public String toString() {
        return "Clams with sandy textures";
    }
}
