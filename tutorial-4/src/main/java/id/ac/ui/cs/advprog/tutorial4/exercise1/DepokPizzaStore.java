package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheezyPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.MixedPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.PorkPizza;

public class DepokPizzaStore extends PizzaStore {

    @Override
    protected Pizza createPizza(String item) {
        Pizza pizza = null;
        PizzaIngredientFactory ingredientFactory =
                new DepokPizzaIngredientFactory();

        if (item.equals("pork")) {

            pizza = new PorkPizza(ingredientFactory);
            pizza.setName("Depok Style Pork Pizza");

        } else if (item.equals("cheezy")) {

            pizza = new CheezyPizza(ingredientFactory);
            pizza.setName("Depok Style Cheezy Pizza");

        } else if (item.equals("mixed")) {

            pizza = new MixedPizza(ingredientFactory);
            pizza.setName("Depok Style Mixed Pizza");

        }

        return pizza;
    }
}
