package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

public class Pork implements Veggies {

    public String toString() {
        return "Juicy & Tender Steak Ham";
    }
}
