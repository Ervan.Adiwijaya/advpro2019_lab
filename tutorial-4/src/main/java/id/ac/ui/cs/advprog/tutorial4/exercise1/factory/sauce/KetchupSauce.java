package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class KetchupSauce implements Sauce {

    public String toString() {
        return "Ketchup Sauce with Pork";
    }
}
