package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;

public class PorkPizza extends Pizza {

    PizzaIngredientFactory ingredientFactory;

    public PorkPizza(PizzaIngredientFactory ingredientFactory) {
        this.ingredientFactory = ingredientFactory;
    }

    public void prepare() {
        System.out.println("Preparing " + name);
        dough = ingredientFactory.createDough();
        veggies = ingredientFactory.createVeggies();
        cheese = ingredientFactory.createCheese();
    }
}
