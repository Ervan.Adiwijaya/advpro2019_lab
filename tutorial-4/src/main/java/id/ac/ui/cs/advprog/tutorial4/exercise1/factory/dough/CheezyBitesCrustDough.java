package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class CheezyBitesCrustDough implements Dough {

    public String toString() {
        return "CheezyBites Crust Dough";
    }
}
