package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class DepokPizzaStoreTest {
    private DepokPizzaStore newYorkPizzaStore;

    @Before
    public void setUp() {
        newYorkPizzaStore = new DepokPizzaStore();
    }

    @Test
    public void testCreatePorkPizza() {
        Pizza pizza = newYorkPizzaStore.createPizza("pork");
        assertEquals("Depok Style Pork Pizza",pizza.getName());
    }

    @Test
    public void testCreateCheezyPizza() {
        Pizza pizza = newYorkPizzaStore.createPizza("cheezy");
        assertEquals("Depok Style Cheezy Pizza",pizza.getName());
    }

    @Test
    public void testCreateMixedPizza() {
        Pizza pizza = newYorkPizzaStore.createPizza("mixed");
        assertEquals("Depok Style Mixed Pizza",pizza.getName());
    }
}
