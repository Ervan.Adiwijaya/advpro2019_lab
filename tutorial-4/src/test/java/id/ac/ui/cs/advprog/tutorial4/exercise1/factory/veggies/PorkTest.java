package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class PorkTest {
    private Veggies veggies;

    @Before
    public void setUp() {
        veggies = new Pork();
    }

    @Test
    public void testPorkName() {
        assertEquals("Juicy & Tender Steak Ham", veggies.toString());
    }
}