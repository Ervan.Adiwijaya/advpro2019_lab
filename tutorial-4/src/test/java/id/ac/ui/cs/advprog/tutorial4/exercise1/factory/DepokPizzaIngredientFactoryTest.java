package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import org.junit.Before;
import org.junit.Test;

public class DepokPizzaIngredientFactoryTest {

    private DepokPizzaIngredientFactory pizzaFactory;
    private Veggies[] veggiesDepok;

    @Before
    public void setUp() {
        pizzaFactory = new DepokPizzaIngredientFactory();
    }

    @Test
    public void testDoughCreated() {
        assertEquals("CheezyBites Crust Dough",pizzaFactory.createDough().toString());
    }

    @Test
    public void testCheeseCreated() {
        assertEquals("Shredded Cheddar",pizzaFactory.createCheese().toString());
    }

    @Test
    public void testClamsCreated() {
        assertEquals("Clams with sandy textures",pizzaFactory.createClam().toString());
    }

    @Test
    public void testSauceCreated() {
        assertEquals("Ketchup Sauce with Pork",pizzaFactory.createSauce().toString());
    }

    @Test
    public void testVeggiesCreated() {
        veggiesDepok = pizzaFactory.createVeggies();
        assertEquals("Eggplant", veggiesDepok[0].toString());
        assertEquals("Juicy & Tender Steak Ham", veggiesDepok[1].toString());
        assertEquals("Spinach", veggiesDepok[2].toString());
        assertEquals("Black Olives", veggiesDepok[3].toString());
    }
}