package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MiddleEastClamsTest {
    private Clams clam;

    @Before
    public void setUp() {
        clam = new MiddleEastClams();
    }

    @Test
    public void testClamsName() {
        assertEquals("Clams with sandy textures", clam.toString());
    }
}