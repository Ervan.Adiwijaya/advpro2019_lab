package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaIngredientFactoryTest {

    private NewYorkPizzaIngredientFactory pizzaFactory;
    private Veggies[] veggiesNewYork;

    @Before
    public void setUp() {
        pizzaFactory = new NewYorkPizzaIngredientFactory();
    }

    @Test
    public void testDoughCreated() {
        assertEquals("Thin Crust Dough",pizzaFactory.createDough().toString());
    }

    @Test
    public void testCheeseCreated() {
        assertEquals("Reggiano Cheese",pizzaFactory.createCheese().toString());
    }

    @Test
    public void testClamsCreated() {
        assertEquals("Fresh Clams from Long Island Sound",pizzaFactory.createClam().toString());
    }

    @Test
    public void testSauceCreated() {
        assertEquals("Marinara Sauce",pizzaFactory.createSauce().toString());
    }

    @Test
    public void testVeggiesCreated() {
        veggiesNewYork = pizzaFactory.createVeggies();
        assertEquals("Garlic", veggiesNewYork[0].toString());
        assertEquals("Onion", veggiesNewYork[1].toString());
        assertEquals("Mushrooms", veggiesNewYork[2].toString());
        assertEquals("Red Pepper", veggiesNewYork[3].toString());
    }
}