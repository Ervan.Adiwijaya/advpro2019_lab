package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class KetchupSauceTest {
    private Sauce sauce;
    
    @Before
    public void setUp() {
        sauce = new KetchupSauce();
    }

    @Test
    public void testSauceName() {
        assertEquals("Ketchup Sauce with Pork", sauce.toString());
    }
}