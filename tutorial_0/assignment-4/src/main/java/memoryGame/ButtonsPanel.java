package memoryGame;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Random;

/**
 * Class used to make the panel for cards in Card Memory Game.
 * Most of the code was got from here :
 * https://codereview.stackexchange.com/questions/185875/java-swing-memory-game-colors? \
 * utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
 * @author Ervan Adiwijaya Haryadi / 1706074871
 */
public class ButtonsPanel extends JPanel implements ActionListener{

    private static final int ROW = 6;
    private static final int COLUMN = 6;
    private static final int HORIZONTAL_GAP = 5;
    private static final int VERTICAL_GAP = 5;
    private static final int PANEL_BORDER = 5;

    private MainPanel mainPanel;

    private Card[][] cards = null;
    private ArrayList<String> imgLst = new ArrayList<String>();

    private Random random;
    int mouseClick = 0;
    private int score = mouseClick/2;
    private Card selectedCard;
    private Card c1;
    private Card c2;
    private Card c3;

    /**
     * Contstructor of the class.
     */
    public ButtonsPanel(MainPanel main){
        this.mainPanel = main;
        setBackground(Color.WHITE);

        GridLayout layout = new GridLayout(ROW, COLUMN, HORIZONTAL_GAP, VERTICAL_GAP);
        setLayout(layout);
        
        setBorder(BorderFactory.createEmptyBorder(PANEL_BORDER,PANEL_BORDER,PANEL_BORDER,PANEL_BORDER));

        cards = new Card[ROW][COLUMN];
        initImages();
        shuffle();
        setCards();

    }

    /**
     * Method to generate Image object type from image source
     * and adds them to a list of Images.
     */
    public void initImages() {
        for (int i = 1; i <= 18; i++) {
            try {
                imgLst.add("A:/UI/SEMESTER 2/DDP2_TP/assignment-4/src/main/images/uma" + i + ".png");
                imgLst.add("A:/UI/SEMESTER 2/DDP2_TP/assignment-4/src/main/images/uma" + i + ".png");
            } catch (Exception e) {
                System.out.println("ERROR : Some images cannot be loaded. Exiting program . . .");
                System.exit(0);
            }
        }
    }

    /**
     * gets the score (how many times the cards have been clicked).
     */
    public int getScore() {
        return this.mouseClick;
    }

    /**
     * Sets the number of click.
     * Used to reset the mouseClick to zero.
     */
    public void setScore(int score) {
        this.mouseClick = score;
    }
    
    /**
     * Method to implements Button type to cards (makes the card clickable).
     */
    public void setCards() {
        int a = 0;
        for (int row = 0; row < cards.length; row++) {
            for (int column = 0; column < cards[0].length; column++) {
                cards[row][column] = new Card(imgLst.get(a));
                cards[row][column].setSize(new Dimension(50, 100));
                add(cards[row][column]);
                cards[row][column].addActionListener(this);
                a++;
            }
        }
    }

    /**
     * method to shuffle through the Image List.
     */
    public void shuffle() {
        random = new Random();
        for (int i = 0; i < imgLst.size(); i++) {
            int pos = random.nextInt(imgLst.size());
            String temp = imgLst.get(i);
            imgLst.set(i, imgLst.get(pos));
            imgLst.set(pos, temp);
        }
    }

    /**
     * gets the back side (unique image) of the card.
     */
    public void setBack(Card card) {
        card.getBack();
    }

    /**
     * method to "flip" the card when clicked.
     */
    public void showCardImage() {
        for (int row = 0; row < cards.length; row++) {
            for (int column = 0; column < cards[0].length; column++) {
                if (selectedCard == cards[row][column]) {
                    if (!cards[row][column].isCMatched()) {
                        if (!cards[row][column].isCSelected()) {
                            setBack(cards[row][column]);
                            cards[row][column].setSelected(true);
                            mouseClick++;
                            System.out.println(mouseClick);
                        } else {
                            System.out.println("This field is already selected");
                        }
                    } else {
                        System.out.println("This file is already matched.");
                    }
                }
            }
        }
    }

    /**
     * method to hide the cards selected when mismatch happens
     */
    public void hideCardImage() {
        if (mouseClick > 1 && mouseClick % 2 != 0) {
            c1.getFront();
            c2.getFront();
            c1 = null;
            c2 = null;
        }
    }

    /**
     * method to check whether the game has finished,
     * a matching pair of cards,
     * and if the chosen cards are not matching.
     */
    public void check() {
        if (c1.getName().equals(c2.getName())) {
            c1.setMatched(true);
            c1.setVisible(false);
            c2.setMatched(true);
            c2.setVisible(false);
            if (isEndOfGame() == true) {
                int temp = score/2;
                JOptionPane.showMessageDialog(this, "You won in " + temp + " moves !");
            }
            c1 = null;
            c2 = null;
            c3 = null;

            c1 = selectedCard;
            showCardImage();

        } else {
            showCardImage();
            c1.setSelected(false);
            c2.setSelected(false);
            hideCardImage();
            c1 = c3;
            c3 = null;
        }
    }

    /**
     * returns the boolean value of the end of game
     * @return boolean value of game status (finish/not finish)
     */
    public boolean isEndOfGame() {

        for (Card[] cards2d : cards) {
            for (Card cards1d : cards2d) {
                if (cards1d.isCMatched() == false) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * method to flip the selected card(s)
     * and calls the check method to do the checking of matching pairs.
     */
    public void doTurn() {
        if (c1 == null && c2 == null) {
            if (!selectedCard.isCMatched()) {
                c1 = selectedCard;
                showCardImage();
            }
        }
        if (c1 != null && c1 != selectedCard && c2 == null) {
            if (!selectedCard.isCMatched()) {
                c2 = selectedCard;
                showCardImage();

            }
        }
        if (c1 != null && c2 != null && c2 != selectedCard && c3 == null) {
            c3 = selectedCard;
            check();
        }
    }

    Object source;
    @Override
    public void actionPerformed(ActionEvent e) {
        source = e.getSource();                 

        this.mainPanel.getScore();
        selectedCard = (Card) source;

        doTurn();
    }
}