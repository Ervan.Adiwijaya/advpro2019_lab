package memoryGame;

import javax.swing.*;
import java.awt.*;
/**
 * Class used to make the main panel in Card Memory Game.
 * Most of the code was got from here :
 * https://codereview.stackexchange.com/questions/185875/java-swing-memory-game-colors? \
 * utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
 * @author Ervan Adiwijaya Haryadi / 1706074871
 */
public class MainPanel extends JPanel{

    private BorderLayout borderLayout;
    private BottomPanel bottomPanel;
    private ButtonsPanel buttonsPanel;

    /**
     * constructor of the class
     */
    public MainPanel(){

        borderLayout = new BorderLayout(1,1);

        setLayout(borderLayout);

        buttonsPanel = new ButtonsPanel(this);
        bottomPanel = new BottomPanel(buttonsPanel);

        add(buttonsPanel);
        add(bottomPanel);
        borderLayout.addLayoutComponent(buttonsPanel, BorderLayout.CENTER);
        borderLayout.addLayoutComponent(bottomPanel, BorderLayout.SOUTH);
    }

    /**
     * gets the number of moves done by how many clicks on cards divided by 2.
     */
    public void getScore() {
        bottomPanel.setScore(buttonsPanel.getScore()/2);
    }
}