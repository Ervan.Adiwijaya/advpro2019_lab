package memoryGame;

import java.awt.Dimension;
import java.awt.Image;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.*;

/**
 * Class used to make cards used in Card Memory Game.
 * Most of the code was got from here :
 * https://codereview.stackexchange.com/questions/185875/java-swing-memory-game-colors? \
 * utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
 * @author Ervan Adiwijaya Haryadi / 1706074871
 */

public class Card extends JButton {

    /**
     * A field used to determine if 2 cards are matched.
     */
    private boolean cardMatched;

    /**
     * A field used to determine whether the card selected or not.
     */
    private boolean cardSelected;

    /**
     * A field that contain card's name.
     */
    private String name;

    /**
     * A field that contain front image (common) of card.
     */
    private Image front;

    /**
     * A field that contain back image (unique to 2 cards) of card.
     */
    private Image back;


    /**
     * Constructor of the class.
     */
    public Card(String name)
    {
        try {
            this.back = ImageIO.read(new File(name));
            this.front = ImageIO.read(new File("A:/UI/SEMESTER 2/DDP2_TP/" 
            + "assignment-4/src/main/images/uma0.png"));
            this.name = name;
            this.back = this.back.getScaledInstance(48, 98, java.awt.Image.SCALE_SMOOTH);
            this. front = this.front.getScaledInstance(48, 98, java.awt.Image.SCALE_SMOOTH);
            cardSelected = false;
            cardMatched = false;
            getFront();
        } catch (Exception e) {
            System.out.println("ERROR : One or more card cannot be created.\nExiting program . . .");
            System.exit(0);
        }
    }

    /**
     * Changing the card's icon into the back image (unique).
     */
    public void getBack() {
        this.setIcon(new ImageIcon(back));
    }

    /**
     * Changing the card's icon into the front image (common).
     */
    public void getFront() {
        this.setIcon(new ImageIcon(front));
    }

    /**
     * Gets the card's name.
     * @return String value of card's name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of card's matching parameter.
     * @param boolean boolean value of cards' matchness.
     */
    public void setMatched(boolean matched) {
        cardMatched = matched;
    }

    /**
     * Sets the value of card's selected parameter
     * @param boolean boolean value of card's pick.
     */
    public void setSelected(boolean selected) {
        cardSelected = selected;
    }

    /**
     * Returns boolean value of card's matchness.
     * @return boolean value of matched parameter.
     */
    public boolean isCMatched() {
        if (cardMatched == true) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns boolean value of card's election.
     * @return boolean value of selected parameter.
     */
    public boolean isCSelected() {
        if (cardSelected == true) {
            return true;
        } else {
            return false;
        }
    }
}