package memoryGame;

import javax.swing.*;
import java.awt.*;

/**
 * Class used to run the Card Memory Game.
 * Most of the code was got from here :
 * https://codereview.stackexchange.com/questions/185875/java-swing-memory-game-colors? \
 * utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
 * @author Ervan Adiwijaya Haryadi / 1706074871
 */
public class MemoryGame extends JFrame {

    private static final int WINDOW_WIDTH = 600;
    private static final int WINDOW_HEIGHT = 700;

    private MainPanel mainPanel;

    /**
     * constructor of the class
     */
    public MemoryGame() {

        setSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setTitle("Memory Game");

         mainPanel = new MainPanel();

        add(mainPanel);

        setVisible(true);
    }

    /**
     * main program
     */
    public static void main (String[] args) {
        new MemoryGame();
    }
}