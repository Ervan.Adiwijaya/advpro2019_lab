package memoryGame;


import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;

/**
 * Class used to make panel for utility buttons in Card Memory Game
 * such as restart and exit button.
 * Most of the code was got from here :
 * https://codereview.stackexchange.com/questions/185875/java-swing-memory-game-colors? \
 * utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
 * @author Ervan Adiwijaya Haryadi / 1706074871
 */
public class BottomPanel extends JPanel {
    
    private ButtonsPanel buttons;
    private JButton buttonRestart;
    private JButton buttonExit;
    private JLabel labelScore;
    private BorderLayout borderLayout;
    int score = 0;

    /**
     * Constructor of this class.
     */
    public BottomPanel(ButtonsPanel panel){

        this.buttons = panel;
        borderLayout = new BorderLayout();
        setLayout(borderLayout);
        buttonRestart = new JButton("Restart");
        buttonRestart.addActionListener(new ActionListener(){
        
            @Override
            public void actionPerformed(ActionEvent e) {
                restart();
            }
        });
        buttonRestart.setPreferredSize(new Dimension(150,50));
        
        buttonExit = new JButton("Exit");
        buttonExit.addActionListener(new ActionListener(){
        
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        buttonExit.setPreferredSize(new Dimension(150,50));

        labelScore = new JLabel("" + labelScore, SwingConstants.CENTER);
        labelScore.setPreferredSize(new Dimension(50,50));

        labelScore.setFont(new Font("Courier", Font.BOLD, 16));
        labelScore.setText("0");

        add(buttonRestart);
        add(labelScore);
        add(buttonExit);
        borderLayout.addLayoutComponent(buttonRestart, BorderLayout.WEST);
        borderLayout.addLayoutComponent(labelScore,BorderLayout.CENTER);
        borderLayout.addLayoutComponent(buttonExit,BorderLayout.EAST);
    }

    /**
     * sets the display of moves done
     */
    public void setScore(int text) {
        labelScore.setText(Integer.toString(text));
    }

    /**
     * method for restarting the Card Memory Game.
     */
    public void restart() {
        this.buttons.setVisible(false);
        this.buttons.removeAll();
        this.buttons.shuffle();
        this.buttons.setCards();
        this.buttons.revalidate();
        this.buttons.setVisible(true);
        this.buttons.setScore(0);
    }
}