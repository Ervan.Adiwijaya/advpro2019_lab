package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {

    ModelDuck() {
        this.setFlyBehavior(new FlyNoWay());
        this.setQuackBehavior(new MuteQuack());
    }
    
    public void display() {
        System.out.println("ORE WA PLASTIC DAAA!");
    }
}
