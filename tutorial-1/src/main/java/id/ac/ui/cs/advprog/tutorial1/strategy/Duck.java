package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    public abstract void display();

    public void swim() {
        System.out.println("I swam");
    }

    public void setFlyBehavior(FlyBehavior f) {
        this.flyBehavior = f;
    }

    public void setQuackBehavior(QuackBehavior q) {
        this.quackBehavior = q;
    }
}
